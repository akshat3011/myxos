integer main()
{
	integer pid;
	integer a;
	integer j;
	print ("Before Fork");
	j=0;
	while(j<3) do
		pid = Fork();
		j=j+1;
	endwhile;
	if(pid < 0) then 
	a=Exec("even.xsm");
	endif;
	print ("After Fork");
    integer i;
	i=0;
	while(i<10) do
		print(i);
		print("  ");
	i=i+1;
	endwhile;
	
	return 0;
}

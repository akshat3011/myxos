decl
	integer file1;
	integer status;
	integer file2;
enddecl
integer main()
{
	status = Create("myfile1.dat");
	print(status);
	print("file1 created");
	status = Create("myfile2.dat");
	print(status);
    print ("file2 created ");
    file1 = Open("myfile1.dat");
	print(file1);
	print("file1 opened");
	file2 = Open("myfile2.dat");
	print(file2);
	print ("file2 opened");
	status = Delete("myfile2.dat");
	print(status);
	status = Close(file1);
	print(status);
	print ("file1 closed");
	status = Close(file2);
	print(status);
	print ("file2 closed");
	status = Delete("myfile1.dat");
	print(status);
	
	print("files deleted");
	return 0;
}
